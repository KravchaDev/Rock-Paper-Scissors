#игра основанная на циклах

rock = "
    _______
---'   ____)
      (_____)
      (_____)
      (____)
---.__(___)
\n
"

paper = "
    _______
---'   ____)____
          ______)
          _______)
         _______)
---.__________)
\n
"

ship = "
    _______
---'   ____)____
          ______)
       __________)
      (____)
---.__(___)
\n
" 

print("Приветствую тебя в сессии игры Камень-Ножницы-Бумага: \n 1 - Камень  2 - Ножницы  3 - Бумага\n")
humanchoice = gets.chomp()

system('clear')

rnd = Random.new
computerchoice = rnd.rand(1..3)

computerchoice = computerchoice.to_s

result = humanchoice+computerchoice

#comment human choice

if (humanchoice == "1")
  print ("Ты выбрал: Камень\n" + rock)
end

if (humanchoice == "2")
  print ("Ты выбрал: Ножницы\n" + ship)
end

if (humanchoice == "3")
  print ("Ты выбрал: Бумагу\n" + paper)
end

#comment computer choice

if (computerchoice == "1")
  print ("Компьютер выбрал: Камень\n" + rock)
end

if (computerchoice == "2")
  print ("Компьютер выбрал: Ножницы\n" + ship)
end

if (computerchoice == "3")
  print ("Компьютер выбрал: Бумагу\n" + paper)
end

#who win?

if (result == "11")
  print("Ничья") 
end

if (result == "22")
  print("Ничья") 
end

if (result == "33")
  print("Ничья") 
end

if (result == "12")
  print("Победа за тобой!")
end

if (result == "23")
  print("Победа за тобой!")
end

if (result == "31")
  print("Победа за тобой!")
end

if (result == "13")
  print("Победа за компьютером")
end

if (result == "32")
  print("Победа за компьютером")
end

if (result == "21")
  print("Победа за компьютером")
end